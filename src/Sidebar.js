import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import './Sidebar.css';

export default props => {
  return (
    <Menu>
      <a className="menu-item" href="/">
        Home
      </a>
      <a className="menu-item" href="/salads">
        Dispositivos
      </a>
      <a className="menu-item" href="/pizzas">
        Eventos
      </a>
      <a className="menu-item" href="/desserts">
        Posiciones
      </a>
    </Menu>
  );
};